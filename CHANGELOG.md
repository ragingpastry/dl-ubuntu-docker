# [1.1.0](https://gitlab.com/ragingpastry/dl-ubuntu-docker/compare/v1.0.0...v1.1.0) (2019-08-23)


### Features

* add git ([7f9d74c](https://gitlab.com/ragingpastry/dl-ubuntu-docker/commit/7f9d74c))

# 1.0.0 (2019-08-23)


### Features

* initial release ([0f96fbb](https://gitlab.com/ragingpastry/dl-ubuntu-docker/commit/0f96fbb))
